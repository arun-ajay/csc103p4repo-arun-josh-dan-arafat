#include <cstdio>
#include <getopt.h> // to parse long arguments.
#include <string>
using std::string;
#include <iostream>
using namespace std;
#include <set>
using std::set;
using std::multiset;
#include <strings.h>
using std::swap;
#include <vector>

static const char* usage =
"Usage: %s [OPTIONS]...\n"
"Limited clone of sort.  Supported options:\n\n"
"   -r,--reverse        Sort descending.\n"
"   -f,--ignore-case    Ignore case.\n"
"   -u,--unique         Don't output duplicate lines.\n"
"   --help              Show this message and exit.\n";

struct igncaseComp {
	bool operator()(const string& s1, const string& s2) {
		return (strcasecmp(s1.c_str(),s2.c_str()) < 0);
	}
};
/* NOTE: set<string,igncaseComp> S; would declare a set S which
 * does its sorting in a case-insensitive way! */
void sort(vector<string>& A);
size_t indexOfSmallest(const vector<string>& A, size_t start);
void sortDescending(vector<string>& A);
size_t indexOfLargest(const vector<string>& A, size_t start);

int main(int argc, char *argv[]) {
	// define long options
	static int descending=0, ignorecase=0, unique=0;
	static struct option long_opts[] = {
		{"reverse",       no_argument,   0, 'r'},
		{"ignore-case",   no_argument,   0, 'f'},
		{"unique",        no_argument,   0, 'u'},
		{"help",          no_argument,   0, 'h'},
		{0,0,0,0}
	};
	// process options:
	char c;
	int opt_index = 0;
	while ((c = getopt_long(argc, argv, "rfuh", long_opts, &opt_index)) != -1) {
		switch (c) {
			case 'r':
				descending = 1;
				break;
			case 'f':
				ignorecase = 1;
				break;
			case 'u':
				unique = 1;
				break;
			case 'h':
				printf(usage,argv[0]);
				return 0;
			case '?':
				printf(usage,argv[0]);
				return 1;
		}
	}
	/* TODO: write me... */
	if(descending != 1)
	{
	string n;
	vector<string> inputVec;
	while (getline(cin,n)){/* we use getline on top of cin in order to treat entire lines
		as strings instead of just seperate words*/
		inputVec.push_back(n);
	}
	sort(inputVec);
	for (size_t i = 0; i < inputVec.size(); i++) {
		cout << inputVec[i] << "\n";
	}
}

	if(descending == 1)
	{
		string n;
		vector<string> inputVec;
		while (getline(cin,n)){/* we use getline on top of cin in order to treat entire lines
		as strings instead of just seperate words*/
		inputVec.push_back(n);
			}
			sortDescending(inputVec);
			for (size_t i = 0; i < inputVec.size(); i++) {
				cout << inputVec[i] << "\n";
				}
			}
	return 0;
}

size_t indexOfSmallest(const vector<string>& A, size_t start)
{
	size_t minIndexSoFar = start; /* index of smallest element in
	                                 vector A[start..i] */
	for (size_t i = start+1; i < A.size(); i++) {
		if (A[minIndexSoFar] > A[i])/* Checks every element in the vector[start..i] in order to find the smallest element from vector[start..i]*/
			minIndexSoFar = i;
	}
	return minIndexSoFar;
}

void sort(vector<string>& A)
{
	for (size_t sortLine = 0; sortLine < A.size()-1; sortLine++) {
		/* finds index of smallest thing starting at sortline(done by indexOfSmallest), then
		 * swap with value at sortline. */
		size_t iSmallest = indexOfSmallest(A,sortLine);
		swap(A[iSmallest],A[sortLine]);
	}
}

size_t indexOfLargest(const vector<string>& A, size_t start)
{
	size_t maxIndexSoFar = start; /* index of largest element in
	                                 vector A[start..i] */
	for (size_t i = start+1; i < A.size(); i++) {
		if (A[maxIndexSoFar] < A[i])/* Checks every element in the vector[start..i] in order to find the largest element from vector[start..i]*/
			maxIndexSoFar = i;
	}
	return maxIndexSoFar;
}

void sortDescending(vector<string>& A)
{
	for (size_t sortLine = 0; sortLine < A.size()-1; sortLine++) {
		/* finds index of largest thing starting at sortline(done by indexOfLargest), then
		 * swap with value at sortline. */
		size_t iLargest = indexOfLargest(A,sortLine);
		swap(A[iLargest],A[sortLine]);
	}
}