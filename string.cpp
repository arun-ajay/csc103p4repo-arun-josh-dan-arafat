#include <iostream>
#include <string>
#include <vector>
using namespace std;

int main()
{
	vector<string> V;
	string obtain;
	while(cin >> obtain)
	{
		V.push_back(obtain);
	}
	cout << "Turning to uppercase" << endl << endl;
	for (int i = 0; i < V.size(); i++)
	{
		for (int j = 0; j < V[i].length(); j++)
		{
			if(V[i][j] >= 'a' && V[i][j] <= 'z')
			{
				V[i][j] -= 32;
			}
		}
		for (int j = 0; j < V[i].length(); j++)
		{
			cout << V[i][j];
		}
		cout << endl;
	}

	return 0;
}
