/*
 * CSc103 Project 3: unix utilities
 * See readme.html for details.
 * Please list all references you made use of in order to complete the
 * assignment: your classmates, websites, etc.  Aside from the lecture notes
 * and the book, please list everything.  And remember- citing a source does
 * NOT mean it is okay to COPY THAT SOURCE.  What you submit here **MUST BE
 * YOUR OWN WORK**.
 * References:
 *	None
 *
 * Finally, please indicate approximately how many hours you spent on this:
 * #hours: 15
 *
 * Made by: Arun Ajay 
 */
#include <iostream>
#include <string>
using std::string;
#include <set>
using std::set;
#include <getopt.h> // to parse long arguments.
#include <cstdio> // printf
using std::cin;
using std::endl;
using std::cout;
static const char* usage =
"Usage: %s [OPTIONS]...\n"
"Limited clone of wc.  Supported options:\n\n"
"   -c,--bytes            print byte count.\n"
"   -l,--lines            print line count.\n"
"   -w,--words            print word count.\n"
"   -L,--max-line-length  print length of longest line.\n"
"   -u,--uwords           print unique word count.\n"
"   --help          show this message and exit.\n";

int main(int argc, char *argv[])
{
	/*STRING COLLECTION*/
	string input_string; //Entire string
        string parsed_string;//Part of string(happens when\n occurs)
        bool newflag = false;//Becomes true when it finds a \n
        int string_loop_count = 0; //this differentiates between echo and echo -e
        while (getline(cin,parsed_string))
        {
                if(newflag == true)
                        input_string += "\\n";
                input_string +=  parsed_string;
                newflag = true;
		string_loop_count ++;
        }




	// define long options
	static int charonly=0, linesonly=0, wordsonly=0, uwordsonly=0, longonly=0;
	static struct option long_opts[] = {
		{"bytes",           no_argument,   0, 'c'},
		{"lines",           no_argument,   0, 'l'},
		{"words",           no_argument,   0, 'w'},
		{"uwords",          no_argument,   0, 'u'},
		{"max-line-length", no_argument,   0, 'L'},
		{"help",            no_argument,   0, 'h'},
		{0,0,0,0}
	};
	// process options:
	char c;
	int opt_index = 0;
	while ((c = getopt_long(argc, argv, "clwuLh", long_opts, &opt_index)) != -1) {
		switch (c) {
			case 'c':
				charonly = 1;
				break;
			case 'l':
				linesonly = 1;
				break;
			case 'w':
				wordsonly = 1;
				break;
			case 'u':
				uwordsonly = 1;
				break;
			case 'L':
				longonly = 1;
				break;
			case 'h':
				printf(usage,argv[0]);
				return 0;
			case '?':
				printf(usage,argv[0]);
				return 1;
		
		}
	}
	
	if(linesonly == 1)
	{       int line_count = 0;
		for (int i = 0; i < input_string.length(); i++) // count lines
		{	
			if (string_loop_count == 1)
			{break;}

			if (i+1 != input_string.length())
			{
				if(input_string[i] == '\\' && input_string[i+1] == 'n')
				{line_count ++;}
			}
			
		}

		cout <<line_count << "\t";
	}
	
	if(wordsonly == 1)
	{
		int word_count = 0;
		bool newline_break = false;
		bool word_detection = false;
		for (int i = 0; i < input_string.length(); i++)
		{
			/*if (input_string[i] == '\\' && input_string[i+1] ==  'n')
			{
				cout << endl;
				i++;
				continue;
			}*/
			
			cout << input_string[i];

		}
	/*	for (int i = 0; i < input_string.length(); i++)
		{
			while(input_string[i] == ' ')
			{
				i++;
			}
			while(input_string[i] != ' ' && i < input_string.length())
			{
				if(input_string[i] == '\\' && input_string[i+1] == 'n')
				{
					i ++;
					newline_break = true;
					break;
				}
				else
				{
					word_detection = true;
				}
				i++;
			}
			if(newline_break ==  true && word_detection == false)
			{
				newline_break = false;
				continue;
			}
			else
			{
				word_count ++;
				newline_break = false;
				word_detection = false;
			}

		}
		cout << word_count << "\t";*/
	}


			



	if(charonly == 1)
	{	int newline_count = 0;
		for(int i = 0; i < input_string.length(); i++)
		{
			if (input_string[i] == '\\' && input_string[i+1] == 'n')
				newline_count ++;
		}
		cout << input_string.length() - newline_count << "\t";
	}
	

	/* TODO: write me... */
	if (charonly == 0  && 
	    linesonly == 0  && 
	    wordsonly == 0  && 
	    uwordsonly == 0 &&
	    longonly == 0) //if no argument given, then do #line, #word, #bytes
		{
			//COUNTS
			char c;
			int line_count = 1;
			int word_count = 0;
			int byte_count = 0;
			
			
			for (int i = 0; i < input_string.length(); i++) // count lines
			{	
				if (string_loop_count == 1)
				{break;}

				if (i+1 != input_string.length())
				{
					if(input_string[i] == '\\' && input_string[i+1] == 'n')
					{line_count ++;}
				}
			
			}

			cout << line_count << endl;

		}

	return 0;
}
